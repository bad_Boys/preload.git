

/**
 * uni-app 缓存方法 
 * has  判断缓存数据是否存在
 * getIf 查询缓存数据 自检模式  返回数据 或 当数据过期时 返回false
 * get 获取缓存数据  不检验是否过期
 * set 设置缓存数据 
 * remove 删除缓存数据
 * clear 清空所有缓存数据
 */



/**
 *  判断缓存数据是否存在
 * @param {String} id 
 * @returns Boolean
 */
function has(id) {
  if (!id) return console.error('参数异常，请检查参数')
  if (uni.getStorageSync(id)) {
    return true
  } else {
    return false
  }
}


/**
 * 获取缓存数据  不检验是否过期
 * @param {String} id 
 * @returns 正常：缓存数据 错误：false
 */
function get(id) {
  if (!id) return console.error('参数异常，请检查参数')
  if (uni.getStorageSync(id)) {
    try {
      return uni.getStorageSync(id).data
    } catch (error) {
      // error
      console.error(error)
    }
    return false
  } else {
    return false
  }
}

/**
 * 设置缓存数据 
 * @param {String} id 缓存id
 * @param {Any} data 任何数据类型
 * @param {Number}} expire 多久后过期的毫秒数 默认不过期
 */
function set(id, data = undefined, expire) {
  if (!id && !data) {
    console.error('id或者data数据没传入')
    return false
  }

  if (id && data !== undefined) {
    let cacheData;
    if (expire) {
      cacheData = {
        data,
        expire: new Date().getTime() + expire // 毫秒
      }
    } else {
      cacheData = {
        data,
        expire: 0 // 毫秒
      }
    }
    try {
      uni.setStorageSync(id, cacheData)
    } catch (error) {
      // error
      console.error(error)
    }
    return true
  }
  console.error('未知异常，请检查set方法代码', data)
  return false
}

/**
 * 查询缓存数据 自检模式（返回时检查过期时间）
 * @param {String} id 
 * @returns 正常：缓存数据  错误：（过期或者数据不存在）false
 */
function getIf(id) {
  if (!id) return console.error('参数异常，请检查参数')
  if (uni.getStorageSync(id)) {
    let data;
    try {
      data = uni.getStorageSync(id);
    } catch (error) {
      // error
      console.error(error)
    }

    if (data.expire !== 0 && new Date().getTime() > data.expire) {
      return false
    } else {
      return data.data
    }
  } else {
    return false
  }
}

/**
 * 删除缓存数据
 * @param {String} id 缓存id
 * @returns 
 */
function remove(id) {
  if (!id) return console.error('参数异常，请检查参数')
  if (uni.getStorageSync(id)) {
    try {
      uni.removeStorageSync(id)
    } catch (error) {
      // error
      console.error(error)
    }
    return true
  } else {
    return false
  }
}


function clear() {
  try {
    uni.clearStorageSync();
  } catch (error) {
    // error
    console.error(error)
  }
}


export default {
  clear,
  get,
  set,
  remove,
  has,
  getIf
}



