## 预加载工具

[码云地址]: https://gitee.com/bad_Boys/preload.git

第一次发布这种东西，由于工作的原因没有很多测试的时间，如有问题，望各位手下留情，也可以直接vx联系我，vx：b948993029

注意：预加载http请求最好设置超时时间，如果没有超时时间，预加载会一直处于执行当中，无法对于超时或者错误情况进行处理



- 在开发uniApp的时候我就在想，每个页面的请求为什么非要等到onLoad之后再去请求页面的数据，明明知道要进入哪个页面，并且需要那些数据，为什么不在进入之前就做好加载的请求，这样就避免了进入页面的时候还要去等待数据的返回并且渲染页面，虽然跳转的动画只有300ms，如果响应保持在300ms之内，这种如丝般顺滑我想应该没有人会拒绝的。
- 由于【H5端】刷新时会造成实例丢失，所以在引入了缓存机制，包内默认使用的是uniapp的缓存方式，原生js 或者 原生小程序 请自行修改 包内 cache.js 文件，对外暴露的名字不要改变



###### 

```javascript
// 使用方式
// 1.将需要预加载的http请求函数汇总成一个对象 在 【 new 】 预加载实例的时候作为【 httpObj 】参数传入
// 2. funcPath 参数 是用于获取 【 httpObj 】对象内的请求方法  对象取值的方式
// 3. 预加载实例最好在全局初始化时创建  防止【H5端】刷新时找不到实例
// 例如:
// 		httpObj.abc.abc 
// 		funcPath 的值为 abc.abc 【 httpObj 】不用传，实例内部会自己寻找

// example 示例
// 创建实例
引入预加载工具
import Preload from "@/js_sdk/xiaotian-preload/preload/preload" 

// 创建预加载实例
// httpObj 预加载请求的promise对象 
// DeBug模式 DeBug = true 控制台会打印报错信息  false 不打印错误信息
const preload = new Preload(httpObj:object,DeBug:Boolean) 

// 新增一个预加载请求
preload.addHttp(id:string, funcPath:string, params:Object) 
// 新增一个预加载请求  并立即执行
Preload.runAddHttp(id:string, funcPath:string, params:Object) 

// 删除指定预加载请求
Preload.removeHttp(id:string) 

// 返回一个promise对象，.then 去获取请求结果
Preload.getData(id:string) 

// 手动执行预加载
Preload.send(id:string) 

```





|    方法    |                             参数                             |                       说明                        |
| :--------: | :----------------------------------------------------------: | :-----------------------------------------------: |
|  addHttp   | {string} id 预加载id 要保证唯一性 默认：必传<br /> {string} funcPath 方法路径（对象取值的方式） 默认：必传<br /> {object} params 预加载方法参数 默认：null |      添加预加载请求函数 重复id时后者覆盖前者      |
| runAddHttp | {string} id 预加载id 要保证唯一性<br />  {string} funcPath 方法路径（对象取值的方式） 默认：必传<br /> {object} params 预加载方法参数 默认：null | 添加并立即执行预加载请求函数 重复id时后者覆盖前者 |
| removeHttp |         {string} id 预加载id 要保证唯一性 默认：必传         |                删除指定预加载请求                 |
|  getData   |         {string} id 预加载id 要保证唯一性 默认：必传         |                  获取预加载结果                   |
|    send    |      {string} id 预加载id 要保证唯一性 默认：必传<br />      |                  手动执行预加载                   |

